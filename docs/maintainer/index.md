# Maintainer overview

This section describes the tasks undertaken by the IGWN Conda Distribution
team in maintaining and developing the distribution.

## How to contribute {: #contribute }

**Any IGWN member is welcome** to contribute to the maintenance of the
IGWN Conda Distribution.
If you are willing to help, please ensure you

-   are a member of the
    [`computing/conda`](https://git.ligo.org/computing/conda) GitLab
    project; if you aren't a member you can request to be added
    by emailing <mailto:compsoft@ligo.org>,

-   subscribe to the
    [`conda::requested` label](https://git.ligo.org/computing/sccb/-/labels?search=conda%3A%3Arequested)
    on the [SCCB Requests project](https://git.ligo.org/computing/sccb)
    to ensure that you receive notifications when changes to the distribution
    are requested.

## Defining the distribution {: #defining }

For details on how the contents of the distribution are defined,
see [_Package definitions_](./definitions.md).

## Maintaining the distribution {: #maintaining }

For documentation of how to add to/update the contents of the distribution,
see [_Workflow_](./workflow.md),
and for details of tagging a new release, see [_Releases_](./release.md).

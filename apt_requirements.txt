# this file lists system-level packages required for integration
# testing of the required conda packages

# graphics libraries
libgl1

# ssh is required for MPI
openssh-client

# file is required for ligo.skymap
file

https://git.ligo.org/computing/sccb/-/issues/FIXME

<!-- Edit the labels below as appropriate -->
/label ~"linux-64"
/label ~"osx-64"
/label ~"osx-arm64"
/label ~"win-64"

<!-- probably don't touch these -->
/label ~"target::staging"
/draft

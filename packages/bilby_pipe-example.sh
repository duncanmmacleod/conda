#!/bin/bash
#
# IGWN Conda Distribution test for Bilby Pipe
#
# This test runs the 'zero_noise_with_signal' example
# taken from the bilby_pipe repository, but a different
# test can be specified as desired
#

set -e

PACKAGE="bilby_pipe"
VERSION=$(
conda list ${PACKAGE} --json | \
python -c "import json, sys; print(json.load(sys.stdin)[0]['version'])"
)

EXAMPLE=$1
shift 1 || EXAMPLE="zero_noise/zero_noise_with_signal.ini"

# download the example files for the relevant version
URL="https://git.ligo.org/lscsoft/${PACKAGE}/-/raw/${VERSION}/examples/${EXAMPLE}"
curl -LO ${URL} || {
	echo "download failed, skipping...";
	exit 77;
}

# modify example config
sed -E \
	`# use a single core `\
	-e '/^n-parallel( +)?=/c\n-parallel = 1' \
	`# set a much higher dlogz to get it to finish quickly` \
	-e '/^sampler-kwargs( +)?=/c\sampler-kwargs = {"nlive": 1000, "dlogz": 25}' \
	`# use HL and get data from GWOSC` \
	-e '/^channel-dict( +)?=/cchannel-dict = {"H1": "GWOSC", "L1": "GWOSC"}' \
	`# set label and outdir to something simple` \
	-e '/^label( +)?=/clabel = igwn-conda' \
	-e '/^outdir( +)?=/coutdir = outdir' \
	$(basename ${EXAMPLE}) > bilby_pipe.igwn-conda.ini

# generate workflow
bilby_pipe bilby_pipe.igwn-conda.ini $@

# run the script
bash -e $(test -o xtrace && echo "-x") outdir/submit/bash_igwn-conda.sh

#!/usr/bin/env python
#
# IGWN Conda Distribution tests for PyCm
#

import PyFd as fd
import pytest
import os
import ctypes
import tempfile

filecontents="""#Ensure that cm isn't used
CFG_CMDOMAIN None

# Current logfile path <path>/<cmName>
CFG_PWD %s

#Open up a test frame
FDIN_NEW_FRAME Test 1 1000000000 1 1

#Add Test channels to frames
FDIN_ADD_TEST_CHANNEL V1:test0 "h" 50 32  10.0 0.5 5 32 1.

#Write out the test gwf
FDOUT_FILE %s/Test 1 "*"

#Do a file checksum
FDOUT_FILE_CHECKSUM 3
"""

@pytest.fixture
def fdio_startup():

    #Make a temp directory to give to the fdio server
    temp_dir = tempfile.TemporaryDirectory()

    #Make a temp cfg file to startup the fdio server
    with tempfile.NamedTemporaryFile(suffix=".cfg") as tmp:
        with open(tmp.name, "w", encoding="utf8") as f:
            f.write(filecontents %(temp_dir.name,temp_dir.name))

        #Create fake arguments to pass to the fdio server
        fake_argv = [b'bla', bytes(tmp.name,"utf-8")]
        arr = (ctypes.c_char_p * len(fake_argv))()
        arr[:] = fake_argv

        #Start the FdIOServer
        fdio = fd.FdIONew(len(arr), arr)
        fd.FdIOParseAndIni(fdio)

    return fdio,temp_dir


def test_fdio_server(fdio_startup):

    fdio,temp_dir = fdio_startup

    #Get a frame from the FdIOServer
    frame = fd.FdIOGetFrame(fdio)

    #Print its gps
    serTime = frame.contents.GTimeS

    #Put the gps
    fd.FdIOPutFrame(fdio, frame)

    temp_dir.cleanup()

if __name__ == "__main__":
    import sys
    import pytest
    sys.exit(pytest.main(args=[__file__] + sys.argv[1:]))

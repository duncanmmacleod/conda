#!/bin/bash
#
# IGWN Conda Distribution tests for LSCSoft GLUE
#

# run everything in a temporary directory
PKG_NAME="dttxml"
MOD_NAME="${PKG_NAME}"

# download the original tarball which includes the test directory
VERSION=$(python -c "import ${MOD_NAME}; print(${MOD_NAME}.__version__)")
URL="https://pypi.io/packages/source/${PKG_NAME::1}/${PKG_NAME}/${PKG_NAME}-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/test" "*/conftest.py" || {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
python -m pytest . --cache-clear --no-header -ra -v

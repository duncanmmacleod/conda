#!/bin/bash
#
# IGWN Conda Distribution tests for GWOLLUM
#

# make sure that the test GWF file is installed
test -f ${GWOLLUM_DATA}/V1-hrec-vsr2.gwf

# run test scripts
for gwltest in ${CONDA_PREFIX}/sbin/gwl-test-*; do
    ${gwltest};
done

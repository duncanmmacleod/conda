#!/bin/bash

set -ex

# create some fake data
framecpp_sample --duration 4

# validate FrameL executables
FrChannels Z-ilwd_test_frame-600000000-4.gwf
FrCheck -i Z-ilwd_test_frame-600000000-4.gwf
FrCopy -i Z-ilwd_test_frame-600000000-4.gwf -o copy.gwf
FrDiff -i1 Z-ilwd_test_frame-600000000-4.gwf -i2 copy.gwf -d 1
FrDump -i Z-ilwd_test_frame-600000000-4.gwf -t Z0:RAMPED_REAL_4_1
FrCopy -i Z-ilwd_test_frame-600000000-4.gwf -o copy2.gwf -f 0 -l 1 -r 8
FrTrend -s 1 -p trend- -d 4 -c Z0:RAMPED_REAL_4_1 -f 600000000 -l 600000008 copy2.gwf
